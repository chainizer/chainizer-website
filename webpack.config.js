const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const pkg = require('./package.json');

function htmlWebpackPluginFactory(page) {
    return new HtmlWebpackPlugin({
        template: `src/${page}.ejs`,
        chunks: ['vendor', page],
        filename: `${page}.html`,
        minify: {
            collapseInlineTagWhitespace: false,
            collapseWhitespace: true
        },
        pkg
    });
}

module.exports = {
    devtool: ' source-map',
    entry: {
        'index': './src/index.js'
    },
    output: {
        path: path.join(__dirname, 'public'),
        filename: '[name].[hash].js'
    },
    plugins: [
        new CleanWebpackPlugin(['public']),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default']
        }),
        new ExtractTextPlugin('[name].[hash].css'),
        htmlWebpackPluginFactory('index'),
        new CopyWebpackPlugin([
            {from: './src', to: path.join(__dirname, 'public'), toType: 'dir'}
        ], {
            ignore: ['*.js', '*.json', '*.ejs', '*.less', '*.css']
        })
    ],
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/, loader:
                    'babel-loader'
            },
            {
                test: /\.(eot|woff|woff2|ttf|svg)$/,
                loader: 'url-loader?limit=10000&name=[name]-[hash].[ext]'
            },
            {
                test: /\.(png|jpg)$/,
                loader: 'file-loader?name=[name]-[hash].[ext]'
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({fallback: 'style-loader', use: 'css-loader'})
            },
            {
                test: /\.s[ac]ss$/,
                use: ExtractTextPlugin.extract({
                    use: [{
                        loader: "css-loader"
                    }, {
                        loader: "sass-loader"
                    }],
                    fallback: "style-loader"
                })
            }
        ]
    },
    devServer: {
        https: true,
        contentBase: 'src/public',
        port: 9000
    }
};
